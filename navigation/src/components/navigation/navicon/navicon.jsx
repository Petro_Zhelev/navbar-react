import React from 'react'

import './navicon.css'

const Navicon = ({href, logo, text, input}) => {
	return (
		<>
		<div className="nav-icon-wrapper">
		 <a href={href}>
		<div className="nav-icon">
		   {logo}
		</div>
		<div className="nav-icon__text">{input === '' ? null : <div className='input-holder'>{input}</div>}{text}  {/*switcher === '' ?  null : <div>{switcher}</div>*/}
        </div>
		</a>
		</div>
		</>
	)
}

export default Navicon;