import React from 'react'
import Sidearrow from './sidearrow'
import UserName from './username/username'
import UserNameInfo from './usernameinfo/usernameinfo'
import ModeSwitch from './mode_switch/modeswitch'
import Navicon from './navicon/navicon'
import Input from './input/input'

import {ReactComponent as Bar} from '../../img/bar_chart.svg'
import {ReactComponent as Home} from '../../img/home.svg'
import {ReactComponent as Lense} from '../../img/lense.svg'
import {ReactComponent as Notification} from '../../img/notification_on.svg'
import {ReactComponent as Pie} from '../../img/pie_chart.svg'
import {ReactComponent as Package} from '../../img/package.svg'
import {ReactComponent as Logout} from '../../img/log_out.svg'
import './navigation.css'

const Navigation = () => {
	return(
<div className='navigationHolder'>
	
	<Sidearrow></Sidearrow>
	<div className="top-and-bottom-holder">
<div className="top">
	<div className='username-wrapper'>
		<UserName/>
		<UserNameInfo/>
	</div>
	
	<div className="navIcons">
	<Navicon logo = {<Lense/>} input = {<Input/>} text='Search...'/>
	<Navicon href = "" logo = {<Home/>} text='Dashboard'/>
	<Navicon href = "" logo = {<Bar/>} text='Revenue'/>
	<Navicon href = "" logo = {<Notification/>} text='Notifications'/>
	<Navicon href = "" logo = {<Pie/>} text='Analytics'/>
	<Navicon href = "" logo = {<Package/>} text='Inventory'/>
	</div>
</div>
<div className="bottom">
    <Navicon href = "" logo = {<Logout/>} text='Logout'/>
    <ModeSwitch/>
</div>
</div>

</div>
	)
}

export default Navigation;