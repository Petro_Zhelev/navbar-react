import React from 'react'
import './input.css'

const Input = () =>{
	return (
		<input className = 'search' type="text" placeholder='Search...' />
	)
}

export default Input