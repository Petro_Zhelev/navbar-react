import React from 'react'
import light from '../../../img/sunicon.svg'
import greysun from '../../../img/greysun.svg'
import dark from '../../../img/Icon.svg'
import './modeswitch.css'

const ModeSwitch = () => {
	return (
		<div className='mode-wrapper'>
		<div className="mode__pre-text"><img className='grey-sun' src={greysun} alt="ligt mode logo" />
		    <div className="mode-text">Light mode</div>
		</div>
		<div className="switch">
			<div className="radio-wrapper">
		        <input id='light' type="radio" name='mode'  checked/>	
		        <label for='light'><div className='img-wrapper light'><img src={light} alt="light mode" /></div></label>
		    </div>
		    <div className="radio-wrapper">
		         <input id='dark' type="radio" name='mode'/>	
		        <label for='dark'><div className='img-wrapper dark'><img src={dark} alt="dark mode" /></div></label>
		    </div>
		</div>
		</div>
	)
}

export default ModeSwitch