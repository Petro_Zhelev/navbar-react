import React from 'react'
import './usernameinfo.css'

const UserNameInfo = ()=>{
return (
	<div className="user-name-info">
		<div className="nick">AnimatedFred</div>
		<div className="email">animated@demo.com</div>
	</div>

)
}

export default UserNameInfo